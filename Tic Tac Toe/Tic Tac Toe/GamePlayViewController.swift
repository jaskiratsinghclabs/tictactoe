//
//  ViewController.swift
//  Tic Tac Toe
//
//  Created by Click Labs 65 on 2/2/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

var playerOneWinning = 0
var playerTwoWin = 0
var ties = 0
var totalMatches = 0
var playerOneName = String()
var playerTwoName = String()

class GamePlayViewController: UIViewController {
  
  @IBOutlet weak var winnerLabel: UILabel!
  @IBOutlet weak var blurWindow: UIVisualEffectView!
  var gameMove = 1
  var winner = 0
  var crossIcon = String()
  var noughtsIcon = String()
  
  var gameState = [0,0,0,0,0,0,0,0,0,0] // 0 = not changed,1 = circle,2 = cross
  let winState = [[1,2,3],[4,5,6],[7,8,9],[1,4,7],[2,5,8],[3,6,9],[1,5,9],[3,5,7]] // winning states depicted by button number
  
  @IBAction func buttonPressed(sender: AnyObject) {
    
    if gameState[sender.tag] == 0 && winner == 0 && gameMove < 10 { // if there is no winner or no draw condition
      
      if gameMove % 2 == 0 {
        var image = UIImage(named: "\(selectNoughtsIcon)")
        sender.setImage(image, forState: .Normal)
        gameState[sender.tag] = 1
      } else {
        var image = UIImage(named: "\(selectCrossIcon)")
        sender.setImage(image, forState: .Normal)
        gameState[sender.tag] = 2
      }
      gameMove++
      
      //below checking for combination at every step
      for combination in winState {
        if ( gameState[combination[0]] == gameState[combination[1]] && gameState[combination[1]] == gameState[combination[2]] && gameState[combination[0]] != 0) {
          winner = gameState[combination[0]]
          blurWindow.hidden = false
        }//consecutive position are checked and default/empty is ignored
      }
      
      if ( playerOneName == "" || playerTwoName == "" ) {
        if (winner == 1) {
          winnerLabel.text = "Noughts have won"
          playerTwoWin++
        } else if winner == 2 {
          winnerLabel.text = "Crosses have won"
          playerOneWinning++
        }
      } else {
        if (winner == 1) {
          winnerLabel.text = "\(playerTwoName) have won"
          playerTwoWin++
        } else if (winner == 2) {
          winnerLabel.text = "\(playerOneName) have won"
          playerOneWinning++
        }
      }
      
      UIView.animateWithDuration(0.8, animations: {
        self.blurWindow.center = CGPointMake(self.blurWindow.center.x, self.blurWindow.center.y+400)})
    }
    
    if winner == 0 && gameMove == 10 { //no winner or draw
      blurWindow.hidden = false
      winnerLabel.text = "It's a draw"
      ties++
    }
  }
  
  @IBAction func playAgainPressed(sender: AnyObject) { //reset the data for this screen
    gameMove = 1
    gameState = [0,0,0,0,0,0,0,0,0,0]
    winner = 0
    var button = UIButton()
    for buttonTag in 1...9 {
      button = view.viewWithTag(buttonTag) as UIButton
      button.setImage(nil, forState: .Normal)
    }
    blurWindow.hidden = true
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewDidAppear(animated: Bool) {
    blurWindow.center = CGPointMake(blurWindow.center.x, blurWindow.center.y-400)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
}


//
//  MainScreenController.swift
//  Tic Tac Toe
//
//  Created by Click Labs 65 on 2/4/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//
//Player 1 will be always given choice for cross and player 2 for noughts
//Names will only be transferred if both text fields have value

import Foundation
import UIKit

var selectCrossIcon = "Cross.png"
var selectNoughtsIcon = "Circle.png"
var recordsArray : [String] = []

class MainScreenController: UIViewController , UIPickerViewDelegate {
  
  @IBOutlet weak var pickCrossSignPicker: UIPickerView!
  @IBOutlet weak var pickNoughtsSignPicker: UIPickerView!
  @IBOutlet weak var playerOneTextField: UITextField!
  @IBOutlet weak var playerTwoTextField: UITextField!
  
  let noughtsPickerData = ["circle.png","circle1.png","circle2.png","circle3.png","circle4.png"]
  let crossPickerData = ["cross.png","cross1.png","cross2.png","cross3.png","cross4.png"]
  
  @IBAction func playerOneChooseSignButton(sender: AnyObject) {
    pickNoughtsSignPicker.hidden = false
    pickCrossSignPicker.hidden = true
    self.playerOneTextField.endEditing(true)
  }
  
  @IBAction func playerTwoChooseSignButton(sender: AnyObject) {
    pickCrossSignPicker.hidden = false
    pickNoughtsSignPicker.hidden = true
    self.playerTwoTextField.endEditing(true)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    playerOneWinning = 0
    playerTwoWin = 0
    totalMatches = 0
    ties = 0
    recordsArray = []//resets the data everytime the user is on every screen
  }
  
  func numberOfComponentsInPickerView(pickerView : UIPickerView) -> Int { // number of components in picker
    return 1
  }
  
  func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int { // number of rows in picker
    return noughtsPickerData.count
  }
  
  func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
    return 60
  }
  
  func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
    var myView = UIView(frame: CGRectMake(0 , 0, pickerView.bounds.width - 40 , 50))
    var myImageView = UIImageView(frame: CGRectMake(150 , 0, 50, 50))
    
    if pickerView.tag == 1 {
      var image = noughtsPickerData[row]
      myImageView.image = UIImage(named: "\(image)")
      myView.addSubview(myImageView)
    } else if pickerView.tag == 2 {
      var image = crossPickerData[row]
      myImageView.image = UIImage(named: "\(image)")
      myView.addSubview(myImageView)
    }
    return myView
  }// picker contains a view , view contains UIImageview , and UIImageview is assigned is UIImage based on picker tag
  
  func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    
    if pickerView.tag == 2 {
      selectCrossIcon = crossPickerData[row]
    } else {
      selectNoughtsIcon = noughtsPickerData[row]
    }
  }// on selecting picker row based on picker tag
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if sender?.tag != 230 {
      var gamePlayClassObject = segue.destinationViewController as GamePlayViewController
      playerOneName = playerOneTextField.text
      playerTwoName = playerTwoTextField.text
    }
  }//passing values to ViewController*/
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func viewWillAppear(animated: Bool) {
    if var storedList: AnyObject = NSUserDefaults.standardUserDefaults().objectForKey("StoredRecordsList") {
      
      for i in 0...storedList.count-1 {
        recordsArray.append(storedList[i] as String)
      }
    }
  }
}
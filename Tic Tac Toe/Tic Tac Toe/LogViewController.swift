//
//  LogViewController.swift
//  Tic Tac Toe
//
//  Created by Click Labs 65 on 2/9/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class LogViewController: UIViewController , UITableViewDelegate {
  var updatedRecords = [String]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return recordsArray.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cellIdentifier = "cell"
    var cell : UITableViewCell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as UITableViewCell
    cell.textLabel?.text = "\(updatedRecords[indexPath.row] as NSString)"
    
    var image = UIImage(named: "VersusLogo.png")
    cell.imageView?.image = image
    return cell
  }
  
  override func viewWillAppear(animated: Bool) {   //function updates the table view everytime bar button is pressed
    
    if var storedList: AnyObject = NSUserDefaults.standardUserDefaults().objectForKey("StoredRecordsList") {
      for numbers in stride(from : Int(storedList.count-1) , to : -1 , by: -1){
        updatedRecords.append(storedList[numbers] as String)
      }
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
}

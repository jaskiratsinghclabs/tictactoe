//
//  ScoreViewController.swift
//  Tic Tac Toe
//
//  Created by Click Labs 65 on 2/4/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

import UIKit

class ScoreViewController: UIViewController {
  
  @IBOutlet weak var playerOneWinLabel: UILabel!
  @IBOutlet weak var playerOneLossLabel: UILabel!
  @IBOutlet weak var playerTwoWinLabel: UILabel!
  @IBOutlet weak var playerTwoLossLabel: UILabel!
  @IBOutlet weak var tieLabel: UILabel!
  @IBOutlet weak var totalMatchLabel: UILabel!
  @IBOutlet weak var playerOneNameLabel: UILabel!
  @IBOutlet weak var playerTwoNameLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    totalMatches = playerOneWinning + playerTwoWin + ties
    playerOneWinLabel.text = String(playerOneWinning)
    playerTwoWinLabel.text = String(playerTwoWin)
    playerOneLossLabel.text = String(playerTwoWin)
    playerTwoLossLabel.text = String(playerOneWinning)
    tieLabel.text = String(ties)
    totalMatchLabel.text = String(totalMatches)
    if ( playerOneName == "" || playerTwoName == "" ){ // default value Player 1 and Player 2
    } else {
      playerOneNameLabel.text = playerOneName
      playerTwoNameLabel.text = playerTwoName
    }
  }
  
  @IBAction func saveAndViewLogButton(sender: AnyObject) {
    
    if ( playerOneName == "" || playerTwoName == "" ) {
      
      if playerOneWinning > playerTwoWin {
        recordsArray.append("Player 1\nPlayer 2 \nWinner:Player 1 Wins:\(playerOneWinning) Ties:\(ties) Total Matches:\(totalMatches)")
      } else {
        recordsArray.append("Player 1\nPlayer 2 \nWinner:Player 2 Wins:\(playerOneWinning) Ties:\(ties) Total Matches:\(totalMatches)")
      }
    } else {
      if playerOneWinning > playerTwoWin {
        recordsArray.append("\(playerOneName)\n\(playerTwoName)\nWinner:\(playerOneName)\nWins:\(playerOneWinning) Ties:\(ties) Total Matches:\(totalMatches)")
      } else {
        recordsArray.append("\(playerOneName)\n\(playerTwoName)\nWinner:\(playerOneName)\nWins:\(playerTwoWin) Ties:\(ties) Total Matches:\(totalMatches)")
      }
    }
    
    let StoredRecordsList = recordsArray
    NSUserDefaults.standardUserDefaults().setObject(StoredRecordsList, forKey: "StoredRecordsList")
    NSUserDefaults.standardUserDefaults().synchronize()
  }
  
  override func supportedInterfaceOrientations() -> Int {
    return UIInterfaceOrientationMask.Portrait.rawValue.hashValue
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
}
